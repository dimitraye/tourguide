package tourGuide;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.junit.Test;

import gpsUtil.GpsUtil;
import gpsUtil.location.VisitedLocation;
import org.springframework.context.i18n.LocaleContextHolder;
import rewardCentral.RewardCentral;
import tourGuide.helper.InternalTestHelper;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tourGuide.service.UserService;
import tourGuide.DTO.AttractionDTO;
import tourGuide.model.user.User;
import tripPricer.Provider;

public class TestTourGuideService {

	@Test
	public void getUserLocation() {
		//Configuration locale
		LocaleContextHolder.getLocale().setDefault(new Locale("en", "US"));
		//Initialisation des services
		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
		UserService userService = new UserService();
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService, userService);
		//Création d'un utilisateur
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		//Génération d'un historique de location
		userService.generateUserLocationHistory(user);
		//Suivi de l'emplacement de l'utilisateur
		VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user);
		//Arrêt du Suivi de l'Emplacement
		tourGuideService.tracker.stopTracking();
		//Assertion
		assertTrue(visitedLocation.userId.equals(user.getUserId()));
	}
	
	@Test
	public void addUser() {
		//Initialisation des services
		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
		UserService userService = new UserService();
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService, userService);
		//Création d'utilisateurs factices
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");
		//Ajout d'Utilisateurs
		userService.addUser(user);
		userService.addUser(user2);
		//Récupération d'Utilisateurs
		User retrivedUser = userService.getUser(user.getUserName());
		User retrivedUser2 = userService.getUser(user2.getUserName());
		//Arrêt du Suivi de l'Emplacement
		tourGuideService.tracker.stopTracking();
		//Assertions
		assertEquals(user, retrivedUser);
		assertEquals(user2, retrivedUser2);
	}
	
	@Test
	public void getAllUsers() {
		//Initialisation des services
		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
		UserService userService = new UserService();
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService, userService);
		//Création d'utilisateurs factices
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");
		//Ajout d'Utilisateurs
		userService.addUser(user);
		userService.addUser(user2);
		//Récupération de la Liste de Tous les Utilisateurs
		List<User> allUsers = tourGuideService.getAllUsers();
		//Arrêt du Suivi de l'Emplacement
		tourGuideService.tracker.stopTracking();
		//Assertions
		assertTrue(allUsers.contains(user));
		assertTrue(allUsers.contains(user2));
	}
	
	@Test
	public void trackUser() {
		LocaleContextHolder.getLocale().setDefault(new Locale("en", "US"));

		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
		UserService userService = new UserService();
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService, userService);
		
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		userService.generateUserLocationHistory(user);
		VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user);

		///TODO : voir avec le mentor  ---> Qu'est-ce que doit faire cette méthode.
		tourGuideService.tracker.stopTracking();
		
		assertEquals(user.getUserId(), visitedLocation.userId);
	}
	
	//@Ignore // Not yet implemented
	@Test
	public void get5ClosestAttractions() {
		LocaleContextHolder.getLocale().setDefault(new Locale("en", "US"));

		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
		UserService userService = new UserService();
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService, userService);
		
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		userService.addUser(user);
		VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user);
		
		List<AttractionDTO> attractions = tourGuideService.get5ClosestAttractions(visitedLocation, user);
		
		tourGuideService.tracker.stopTracking();
		
		assertEquals(5, attractions.size());
	}

	@Test
	public void getTripDeals() {
		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
		UserService userService = new UserService();
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService, userService);
		
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		userService.addUser(user);
		List<Provider> providers = tourGuideService.getTripDeals(user);
		
		tourGuideService.tracker.stopTracking();

		//TODO : why expexted 10, en parler avec le mentor
		assertEquals(5, providers.size());
	}
	
	
}
