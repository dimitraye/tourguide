package tourGuide.controller;

import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.jsoniter.output.JsonStream;

import gpsUtil.location.VisitedLocation;
import tourGuide.service.TourGuideService;
import tourGuide.service.UserService;
import tourGuide.DTO.AttractionDTO;
import tourGuide.model.user.User;
import tripPricer.Provider;


/**
 * The TourGuideController class provides RESTful web service endpoints for interacting with the TourGuide application.
 * It allows clients to access information such as user location, closest tourist attractions, and trip deals.
 */
@Slf4j
@RestController
public class TourGuideController {

	@Autowired
	TourGuideService tourGuideService;

    @Autowired
    UserService userService;

    /**
     * Retrieves a greeting message from TourGuide.
     * @return A greeting message from the TourGuide application.
     */
    @RequestMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }

    /**
     * Retrieves the location of a user.
     * @param userName The username of the user for whom to retrieve the location.
     * @return The JSON representation of the user's visited location.
     */
    @RequestMapping("/getLocation") 
    public String getLocation(@RequestParam String userName) {
    	VisitedLocation visitedLocation = tourGuideService.getUserLocation(userService.getUser(userName));
		return JsonStream.serialize(visitedLocation.location);
    }
    
    //  TODO: Change this method to no longer return a List of Attractions.
 	//  Instead: Get the closest five tourist attractions to the user - no matter how far away they are.
 	//  Return a new JSON object that contains:
    	// Name of Tourist attraction, 
        // Tourist attractions lat/long, 
        // The user's location lat/long, 
        // The distance in miles between the user's location and each of the attractions.
        // The reward points for visiting each Attraction.
        //    Note: Attraction reward points can be gathered from RewardsCentral

    /**
     * Retrieves information about the five closest tourist attractions to a user's location.
     * @param userName The username of the user for whom to find the closest attractions.
     * @return A ResponseEntity containing a list of AttractionDTO objects representing the closest attractions.
     */
    @RequestMapping("/get5ClosestAttractions")
    public ResponseEntity<List<AttractionDTO>> get5ClosestAttractions(@RequestParam String userName) {
        User user = userService.getUser(userName);
    	VisitedLocation visitedLocation = tourGuideService.getUserLocation(user);
        return new ResponseEntity<>(tourGuideService.get5ClosestAttractions(visitedLocation, user), HttpStatus.OK);

    }

    /**
     * Retrieves a list of trip deals for a user.
     * @param userName The username of the user for whom to get trip deals.
     * @return A ResponseEntity containing a list of Provider objects representing the trip deals.
     */
    @RequestMapping("/getTripDeals")
    public ResponseEntity<List<Provider>> getTripDeals(@RequestParam String userName) {
    	List<Provider> providers = tourGuideService.getTripDeals(userService.getUser(userName));
        return new ResponseEntity<>(providers, HttpStatus.OK);
    }

}