package tourGuide.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import rewardCentral.RewardCentral;
import tourGuide.model.user.User;
import tourGuide.model.user.UserReward;


/**
 * The RewardsService class provides methods to calculate rewards for users.
 */
@Service
public class RewardsService {
    private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;

	// proximity in miles
    private int defaultProximityBuffer = 10;
	private int proximityBuffer = defaultProximityBuffer;
	private int attractionProximityRange = 200;
	private final GpsUtil gpsUtil;
	private final RewardCentral rewardsCentral;

	ExecutorService executorService = Executors.newFixedThreadPool(44);


	public RewardsService(GpsUtil gpsUtil, RewardCentral rewardCentral) {
		this.gpsUtil = gpsUtil;
		this.rewardsCentral = rewardCentral;
	}

	/**
	 * Sets the proximity buffer for attraction rewards calculations.
	 * @param proximityBuffer The proximity buffer to be set.
	 */
	public void setProximityBuffer(int proximityBuffer) {
		this.proximityBuffer = proximityBuffer;
	}
	
	public void setDefaultProximityBuffer() {
		proximityBuffer = defaultProximityBuffer;
	}

	/**
	 * Calculates rewards for a user based on their visited locations and proximity to attractions.
	 * @param user The user for whom to calculate rewards.
	 */
	public void calculateRewards(User user) {
		List<Attraction> attractions = new CopyOnWriteArrayList<>();
		List<VisitedLocation> visitedLocations = new CopyOnWriteArrayList<>();
		List<UserReward> userRewards = new CopyOnWriteArrayList<>();


		attractions.addAll(gpsUtil.getAttractions());
		visitedLocations.addAll(user.getVisitedLocations());
		userRewards.addAll(user.getUserRewards());

		List<CompletableFuture<User>> completableFutures = new ArrayList<>(); //List to hold all the completable futures

		visitedLocations.forEach(visitedLocation -> {
			attractions.parallelStream().forEach(attraction -> {
				if (user.attractionCouldBeRewarded(attraction)) {

					if (nearAttraction(visitedLocation, attraction)) {
						CompletableFuture<User> future = CompletableFuture.supplyAsync(() ->
										getRewardPoints(attraction, user), executorService)
								.thenApply(points -> {
									UserReward userReward = new UserReward(visitedLocation, attraction, points);
									user.addUserReward(userReward);
									return user;
								});
						completableFutures.add(future);
					}
				}
			});
		});

		CompletableFuture.allOf(completableFutures
						.stream().filter(Objects::nonNull)
						.collect(Collectors.toList())
						.toArray(new CompletableFuture[0]))
				.join();
	}

	/**
	 * Checks if a location is within the proximity range of an attraction.
	 * @param attraction The attraction.
	 * @param location   The location to be checked.
	 * @return true if the location is within attraction proximity, otherwise false.
	 */
	public boolean isWithinAttractionProximity(Attraction attraction, Location location) {
		return getDistance(attraction, location) > attractionProximityRange ? false : true;
	}

	/**
	 * Checks if a user is near an attraction based on their visited location and proximity buffer.
	 * @param visitedLocation The visited location of the user.
	 * @param attraction      The attraction to be checked.
	 * @return true if the user is near the attraction, otherwise false.
	 */
	private boolean nearAttraction(VisitedLocation visitedLocation, Attraction attraction) {
		return getDistance(attraction, visitedLocation.location) > proximityBuffer ? false : true;
	}

	/**
	 * Retrieves the reward points for visiting an attraction for a specific user.
	 * @param attraction The attraction for which reward points are calculated.
	 * @param user       The user for whom the reward points are calculated.
	 * @return The reward points earned.
	 */
	public int getRewardPoints(Attraction attraction, User user) {
		return rewardsCentral.getAttractionRewardPoints(attraction.attractionId, user.getUserId());
	}

	/**
	 * Calculates the distance in statute miles between two locations.
	 * @param loc1 The first location.
	 * @param loc2 The second location.
	 * @return The distance in statute miles.
	 */
	public double getDistance(Location loc1, Location loc2) {
        double lat1 = Math.toRadians(loc1.latitude);
        double lon1 = Math.toRadians(loc1.longitude);
        double lat2 = Math.toRadians(loc2.latitude);
        double lon2 = Math.toRadians(loc2.longitude);

        double angle = Math.acos(Math.sin(lat1) * Math.sin(lat2)
                               + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

        double nauticalMiles = 60 * Math.toDegrees(angle);
        double statuteMiles = STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
        return statuteMiles;
	}

}
