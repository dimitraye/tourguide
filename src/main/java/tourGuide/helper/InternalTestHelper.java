package tourGuide.helper;

/**
 * The InternalTestHelper class provides methods for managing internal testing settings.
 * It allows for adjusting the number of internal users used for testing purposes.
 */
public class InternalTestHelper {

	// Set this default up to 100,000 for testing
	private static int internalUserNumber = 100;
	
	public static void setInternalUserNumber(int internalUserNumber) {
		InternalTestHelper.internalUserNumber = internalUserNumber;
	}
	
	public static int getInternalUserNumber() {
		return internalUserNumber;
	}
}
